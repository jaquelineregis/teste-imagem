import os
import shutil

from django.conf import settings

from django.http import HttpResponse
from django.shortcuts import render
from .forms import ImagemModelForm
from .models import Imagem, ImagemTemporaria, Categoria


# Create your views here.
def configurar(request):

    form = ImagemModelForm(request.POST, request.FILES or None)
    context = {'form_imagem': form}
    try:
        if form.is_valid():
            try:
                print('--------existe----------')
                imagem_existe = Imagem.objects.get(atributo_imagem=form.cleaned_data['atributo_imagem'])
                context['imagem_existe'] = imagem_existe
                Imagem.objects.create(atributo_imagem='erro',
                                      categoria=form.cleaned_data['categoria'])
                ImagemTemporaria.objects.create(atributo_imagem_temp=form.cleaned_data['atributo_imagem'], categoria=form.cleaned_data['categoria'])
                imagem_existe_temp = ImagemTemporaria.objects.first()
                context['imagem_existe_temp'] = imagem_existe_temp
            except:
                print('--------nao existe----------')
                Imagem.objects.create(atributo_imagem=form.cleaned_data['atributo_imagem'], categoria=form.cleaned_data['categoria'])
                return HttpResponse('<h1>Salvo com sucesso <a href="/classeimagem/configurar">voltar</a></h1>')

        elif 'sim' == request.GET.get('sim'):
            print('--------sim----------')
            imagemtemp = ImagemTemporaria.objects.first()
            imagem = Imagem.objects.get(atributo_imagem='erro')
            imagem.atributo_imagem = str(imagemtemp.atributo_imagem_temp)[5:]
            imagem.save()
            os.remove(os.path.join(settings.MEDIA_ROOT, str(imagemtemp.atributo_imagem_temp)))
            imagemtemp.delete()
        elif 'nao' == request.GET.get('nao'):
            print('--------nao----------')
            imagemtemp = ImagemTemporaria.objects.first()
            imagem = Imagem.objects.get(atributo_imagem='erro')
            nome, ext = str(imagemtemp.atributo_imagem_temp)[5:].split('.')
            shutil.move(os.path.join(settings.MEDIA_ROOT, str(imagemtemp.atributo_imagem_temp)), os.path.join(settings.MEDIA_ROOT, nome+str(imagem.id_atributo_imagem)+'.'+ext))
            imagem.atributo_imagem = str(nome+str(imagem.id_atributo_imagem)+'.'+ext)
            imagem.save()
            imagemtemp.delete()
    except:
        return HttpResponse('deu erro')
    return render(request, 'imagem/configurar.html', context)
