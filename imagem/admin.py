from django.contrib import admin
from .models import *

# Register your models here.

class ImagemAdmin(admin.ModelAdmin):
    list_display = ('id_atributo_imagem', 'atributo_imagem', 'categoria')

class ImagemTemporariaAdmin(admin.ModelAdmin):
    list_display = ('atributo_imagem_temp', 'categoria')


admin.site.register(Categoria)
admin.site.register(Imagem, ImagemAdmin)
admin.site.register(ImagemTemporaria, ImagemTemporariaAdmin)