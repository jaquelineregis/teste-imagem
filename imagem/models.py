from django.db import models

# Create your models here.

class Categoria(models.Model):
    categoria = models.CharField(max_length=50, primary_key=True)

    def __str__(self):
        return self.categoria


class Imagem(models.Model):
    id_atributo_imagem = models.AutoField(primary_key=True)
    atributo_imagem = models.ImageField()
    categoria = models.ForeignKey(Categoria)

    class Meta:
        verbose_name = 'Imagem'
        verbose_name_plural = 'Imagens'

    def __str__(self):
        return str(self.atributo_imagem)



class ImagemTemporaria(models.Model):
    atributo_imagem_temp = models.ImageField(upload_to='temp/')
    categoria = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Imagem Temporaria'
        verbose_name_plural = 'Imagens Temporarias'

    def __str__(self):
        return str(self.atributo_imagem_temp)
