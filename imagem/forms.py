from django.forms import ModelForm
from .models import Imagem, Categoria


class ImagemModelForm(ModelForm):
    class Meta:
        model = Imagem
        fields = ['atributo_imagem', 'categoria']

class CategoriaModelForm(ModelForm):
    class Meta:
        model = Categoria
        fields = ['categoria']
